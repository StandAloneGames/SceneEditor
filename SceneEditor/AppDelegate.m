//
//  AppDelegate.m
//  SceneEditor
//
//  Created by lh on 14-1-9.
//  Copyright (c) 2014年 lh. All rights reserved.
//  www.9miao.com

#import "AppDelegate.h"
#import "ASIHTTPRequest.h"
#import "SBJson.h"
@implementation AppDelegate
@synthesize m_static;

@synthesize btype;
@synthesize color;
@synthesize ske;
@synthesize dou;
@synthesize coatt;
@synthesize hp;
@synthesize petarr;

@synthesize tableView;
@synthesize resName;
@synthesize up;
@synthesize down;
@synthesize right;
@synthesize left;
@synthesize gold;
@synthesize exp;
@synthesize sceneId;
@synthesize nextsceneId;
//@synthesize releaseRoles;
@synthesize sceneType;
@synthesize monsterName;
//@synthesize selectPet;
@synthesize sureBtn;
@synthesize m_person;
@synthesize person;

@synthesize tabView;
//@synthesize eid;
@synthesize sceneInfo;

@synthesize tagnum;
@synthesize attack;
@synthesize Attackrange;
@synthesize Attackspeed;
@synthesize baseattack;
@synthesize basecri;
@synthesize basehp;
@synthesize Blockcoefficient;
@synthesize Blockrate;
@synthesize Coefficientofskewness;
@synthesize Crit;
@synthesize Critpower;
@synthesize Critpowercoefficient;
@synthesize Critrate;
@synthesize Critratecoefficient;
@synthesize damagerate;
@synthesize defence;
@synthesize Defensecoefficient;
@synthesize dodge;
@synthesize Dodgecoefficient;
@synthesize escaperate;
@synthesize hit;
@synthesize Hitcoefficient;
//@synthesize level;
@synthesize Movementspeed;
@synthesize parry;
@synthesize sceneName;
@synthesize tenacity;
@synthesize Toughnessproperties;
//@synthesize typeId;
@synthesize countofenemy;
@synthesize enemycount;
@synthesize enemylist;
@synthesize delaytime;
@synthesize samePeople;
@synthesize flag;
@synthesize skillId;
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
    tableView.delegate=self;
    flag=YES;
    tableView.dataSource=self;
    sceneInfo=[[SceneInfo alloc] init];
    sceneInfo.enemyArr=[[NSMutableArray alloc] init];

    person=  [[NSMutableArray alloc]init];
}

-(IBAction)selectBtn:(id)sender{
    
     
    
}
- (NSInteger)numberOfRowsInTableView:(NSTableView*)tv {
    if ([sceneInfo.enemyArr count]==0) {
        return 0;
    }else{
        
        return [[sceneInfo.enemyArr objectAtIndex:[[[enemycount selectedCell] stringValue] intValue]-1] count];
    }
}
-(IBAction)removeSelectTableEnemy:(id)sender{
    NSIndexSet* rows = [tableView selectedRowIndexes];
    if ([rows count] == 0) {
        NSBeep();
        return;
    }
    [[sceneInfo.enemyArr objectAtIndex:[[[enemycount selectedCell] stringValue] intValue]-1] removeObjectsAtIndexes:rows];

    [tableView reloadData];
}
-(IBAction)addSelectTableEnemy:(id)sender{
    NSLog(@"%ld",[enemylist indexOfSelectedItem]);
   NSDictionary *dic=[petarr objectAtIndex:[enemylist indexOfSelectedItem]];
    NSLog(@"%@",[dic objectForKey:@"id"]);

    [[sceneInfo.enemyArr objectAtIndex:[[[enemycount selectedCell] stringValue] intValue]-1] addObject:[dic objectForKey:@"id"]];
    
    [tableView reloadData];
}
- (id)tableView:(NSTableView*)tv objectValueForTableColumn:(NSTableColumn*)tc
            row:(NSInteger)rowIndex {
    if (flag) {
        return [[sceneInfo.enemyArr objectAtIndex:[[[enemycount selectedCell] stringValue] intValue]-1]  objectAtIndex:rowIndex];
    }else{
        return nil;
    }
    
}

- (void)tableView:(NSTableView*)tv setObjectValue:(id)anObject
   forTableColumn:(NSTableColumn*)tc row:(NSInteger)rowIndex {
}
-(IBAction)changeTab:(id)sender{
    return;
    NSIndexSet* rows = [tableView selectedRowIndexes];
    if ([rows count] == 0) {
        NSBeep();
        return;
    }
    sceneInfo=[[SceneInfo alloc] init];
    sceneInfo.enemyArr=[[NSMutableArray alloc] init];
    sceneInfo.resourceName=[resName stringValue];
    sceneInfo.up=[up stringValue];
    sceneInfo.down=[down stringValue];
    sceneInfo.left=[left stringValue];
    sceneInfo.right=[right stringValue];
    sceneInfo.gold=[gold stringValue];
    sceneInfo.exp=[exp stringValue];
    sceneInfo.sceneId=[sceneId stringValue];
    sceneInfo.sceneType=[[sceneType selectedCell] stringValue];
    if ([[[sceneType selectedCell] stringValue] isEqual:@"战役"]){
        sceneInfo.sceneType=@"1";
    }else{
        sceneInfo.sceneType=@"2";
    }
    sceneInfo.nextSceneId=[nextsceneId  stringValue];
//    if ([[[releaseRoles selectedCell] stringValue] isEqual:@"是"]) {
//        sceneInfo.releaseRole=@"1";
//    }
//    else{
//        sceneInfo.releaseRole=@"0";
//    }
    for (int i=0; i<[person count]; i++) {
        Person *enemy=[[Person alloc] init];
        enemy.m_id=[person objectAtIndex:i];
        enemy.isSave=NO;
        [sceneInfo.enemyArr addObject:enemy];
        
        
    }

  //  [eid setStringValue:[person objectAtIndex:[rows firstIndex]]];
   // [selectPet addItemsWithObjectValues:person];
    [tabView selectTabViewItemAtIndex:1];
}
-(IBAction)savePetInfo:(id)sender{
    //||[[basehp stringValue] isEqualToString:@""]||[[basecri stringValue] isEqualToString:@""]||[[baseattack stringValue] isEqualToString:@""]||[[Defensecoefficient stringValue] isEqualToString:@""]||[[Toughnessproperties stringValue] isEqualToString:@""]||[[Dodgecoefficient stringValue] isEqualToString:@""]||[[Blockcoefficient stringValue] isEqualToString:@""]||[[Critratecoefficient stringValue] isEqualToString:@""]||[[Critpowercoefficient stringValue] isEqualToString:@""]||[[Hitcoefficient stringValue] isEqualToString:@""]||[[damagerate stringValue] isEqualToString:@""]||[[escaperate stringValue] isEqualToString:@""]||[[Blockrate stringValue] isEqualToString:@""]||[[Critrate stringValue] isEqualToString:@""]
    if ([[monsterName stringValue] isEqualToString:@""]||[[defence stringValue] isEqualToString:@""]||[[attack stringValue] isEqualToString:@""]||[[hit stringValue] isEqualToString:@""]||[[tenacity stringValue] isEqualToString:@""]||[[dodge stringValue] isEqualToString:@""]||[[parry stringValue] isEqualToString:@""]||[[Crit stringValue] isEqualToString:@""]||[[Critpower stringValue] isEqualToString:@""]||[[Attackrange stringValue] isEqualToString:@""]||[[Movementspeed stringValue] isEqualToString:@""]||[[hp stringValue] isEqualToString:@""]||[[dou stringValue] isEqualToString:@""]||[[ske stringValue] isEqualToString:@""]) {
        NSAlert *alert=[[NSAlert alloc] init];
        //[NSAlert alertWithMessageText:@"111" defaultButton:@"11" alternateButton:@"33" otherButton:@"22"informativeTextWithFormat:@"44"];
        [alert addButtonWithTitle:@"OK"];
        
        
        [alert setMessageText:@"错误"];
        
        [alert setInformativeText:@"错误，值不能为空"];
        
        [alert setAlertStyle:NSWarningAlertStyle];
        [alert beginSheetModalForWindow:_window modalDelegate:self didEndSelector:nil contextInfo:nil];
        return;
    }
    m_person=[[Person alloc] init];
    m_person.skill_Id=[[skillId selectedCell] stringValue];
    m_person.m_hp=[NSNumber numberWithInt:[hp intValue]];
    m_person.dou=[NSNumber numberWithInt:[dou intValue]];
    m_person.ske=[NSNumber numberWithInt:[ske intValue]];
    m_person.btype=[btype stringValue];
    m_person.coatt=[coatt stringValue];
    m_person.color=[color stringValue];
    
    m_person.res_icon=[NSNumber numberWithInt:[resName intValue]];
    m_person.m_name=[monsterName stringValue];
    m_person.m_defense=[NSNumber numberWithInt:[defence intValue]];
    m_person.m_attack=[NSNumber numberWithInt:[attack intValue]];
    m_person.m_hittarget=[NSNumber numberWithInt:[hit intValue]];
    m_person.m_tenacity=[NSNumber numberWithInt:[tenacity intValue]];
    m_person.m_dodge=[NSNumber numberWithInt:[dodge intValue]];
    m_person.m_parry=[NSNumber numberWithInt:[parry intValue]];
    m_person.m_strike=[NSNumber numberWithInt:[Crit intValue]];
    m_person.m_strike_result=[NSNumber numberWithInt:[Critpower intValue]];
    m_person.m_range=[NSNumber numberWithInt:[Attackrange intValue]];
    m_person.m_speed=[NSNumber numberWithInt:[Movementspeed intValue]];
    m_person.base_hp=[NSNumber numberWithInt:[basehp intValue]];
    m_person.base_attack=[NSNumber numberWithInt:[baseattack intValue]];
    m_person.defense_cf=[NSNumber numberWithInt:[Defensecoefficient intValue]];
    m_person.tenacity_cf=[NSNumber numberWithInt:[Toughnessproperties intValue]];
    
    
    m_person.dodge_cf=[NSNumber numberWithInt:[Dodgecoefficient intValue]];
    m_person.parry_cf=[NSNumber numberWithInt:[Blockcoefficient intValue]];
    m_person.deflect_cf=[NSNumber numberWithInt:[Coefficientofskewness intValue]];
    m_person.strike_cf=[NSNumber numberWithInt:[Critratecoefficient intValue]];
    m_person.strike_result_cf=[NSNumber numberWithInt:[Critpowercoefficient intValue]];
    m_person.hitrate_cf=[NSNumber numberWithInt:[Hitcoefficient intValue]];
    m_person.damage_rate=[NSNumber numberWithInt:[damagerate intValue]];
    m_person.avoid_rate=[NSNumber numberWithInt:[escaperate intValue]];
    m_person.parry_rate=[NSNumber numberWithInt:[Blockrate intValue]];
    m_person.strike_rate=[NSNumber numberWithInt:[Critrate intValue]];
    m_person.isSave=YES;
    
    [m_person setProperties];
}
-(IBAction)getPetList:(id)sender{
    tagnum=[sender tag];
    NSLog(@"%ld",tagnum);
    NSURL *url = [NSURL URLWithString:[@"http://127.0.0.1:11008/getmonsterlist?json=" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
	[request setTimeOutSeconds:5];
	[request setDelegate:self];
	[request startAsynchronous];
}
-(IBAction)getModulus:(id)sender{
    tagnum=[sender tag];
//    SBJsonWriter *writer=[[SBJsonWriter alloc] init];
    
  //  NSString *jsonStr=[writer stringWithObject:properties];
    
   // NSLog(@"%@",jsonStr);
    
  //  NSString *str=[[NSString alloc] initWithFormat:@"http://172.16.18.25:11008/addmonster?json=%@",jsonStr];
    //[NSString stringWithFormat:@"http://172.16.18.25:11008/addmonster?json=%@",jsonStr];
 //   NSLog(@"%@",str);
    
    NSURL *url = [NSURL URLWithString:[@"http://127.0.0.1:11008/getmodulus?json=" stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
	[request setTimeOutSeconds:5];
	[request setDelegate:self];
	[request startAsynchronous];
//    NSString *response = [request responseString];
//    NSLog(@"%@",response);
}
- (void)requestFinished:(ASIHTTPRequest *)request

{
    
    // 当以文本形式读取返回内容时用这个方法
    
    NSString *responseString = [request responseString];
    
    NSLog(@"%@",responseString);
//    petarr=[[NSMutableArray alloc] init];
    SBJsonParser *sbjson=[[SBJsonParser alloc] init];
    
    if (tagnum==100) {
        petarr= [sbjson objectWithString:responseString];
        
        for (int i=0; i<[petarr count]; i++) {
            NSDictionary *petDic=[petarr objectAtIndex:i];
            NSLog(@"%@",[petDic objectForKey:@"id"]);
            [enemylist addItemWithObjectValue:[petDic objectForKey:@"pname"]];
        }
        
    }else if(tagnum==200){
        NSMutableDictionary *dic= [sbjson objectWithString:responseString];
        NSLog(@"%@",[dic objectForKey:@"hp"]);
        
        
        [basehp setStringValue:[[dic objectForKey:@"hp"] stringValue]];
        
        [baseattack setStringValue:[[dic objectForKey:@"att"]stringValue]];
        
        [Defensecoefficient setStringValue:[[dic objectForKey:@"def"]stringValue]];
        
        [Toughnessproperties setStringValue:[[dic objectForKey:@"tou"] stringValue]];
        
        [Dodgecoefficient setStringValue:[[dic objectForKey:@"agl"] stringValue]];
        
        [Blockcoefficient setStringValue:[[dic objectForKey:@"par"] stringValue]];
        
        [Coefficientofskewness setStringValue:[[dic objectForKey:@"ske"] stringValue]];
        
        [Critratecoefficient setStringValue:[[dic objectForKey:@"cri"] stringValue]];
        
        [Critpowercoefficient setStringValue:[[dic objectForKey:@"crp"] stringValue]];
        
        [Hitcoefficient setStringValue:[[dic objectForKey:@"dex"] stringValue]];
    }
    
  
    
    
}
-(IBAction)saveFile:(id)sender{
    if ([sceneInfo.up isEqualToString:@""]||[sceneInfo.down isEqualToString:@""]||[sceneInfo.left isEqualToString:@""]||[sceneInfo.right isEqualToString:@""]) {
        [NSAlert alertWithMessageText:@"111" defaultButton:@"11" alternateButton:nil otherButton:nil informativeTextWithFormat:nil];
        return;
    }
    SBJsonWriter *writer=[[SBJsonWriter alloc] init];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:sceneInfo.up forKey:@"up"];
    [dict setValue:sceneInfo.down forKey:@"down"];
    [dict setValue:sceneInfo.left forKey:@"left"];
    [dict setValue:sceneInfo.right forKey:@"right"];
    [dict setValue:sceneInfo.sceneName forKey:@"sceneName"];
    [dict setValue:sceneInfo.gold forKey:@"gold"];
    [dict setValue:sceneInfo.exp forKey:@"exp"];
    [dict setValue:sceneInfo.nextSceneId forKey:@"nextSceneId"];
    [dict setValue:sceneInfo.sceneId forKey:@"sceneId"];
    [dict setValue:sceneInfo.sceneType forKey:@"scenetype"];
  //  [dict setValue:sceneInfo.releaseRole forKey:@"releaserole"];
    NSMutableArray *roleArr=[[NSMutableArray alloc] init];

    
    for (int i=0; i<[sceneInfo.enemyArr count]; i++) {
        [roleArr addObject:((Person*)[sceneInfo.enemyArr objectAtIndex:i]).properties];
        
    }
    [dict setValue:roleArr forKey:@"enemyArr"];
    NSString *str=[writer stringWithObject:dict];
    [str writeToFile:@"/Users/lh/Desktop/b.abc" atomically:YES encoding:NSUTF8StringEncoding error:nil];
    //[writer release];
}
-(IBAction)updateStaticValue:(id)sender{
    if ([[basehp stringValue] isEqualToString:@""]||[[baseattack stringValue] isEqualToString:@""]||[[Defensecoefficient stringValue] isEqualToString:@""]||[[Toughnessproperties stringValue] isEqualToString:@""]||[[Dodgecoefficient stringValue] isEqualToString:@""]||[[Blockcoefficient stringValue] isEqualToString:@""]||[[Critratecoefficient stringValue] isEqualToString:@""]||[[Critpowercoefficient stringValue] isEqualToString:@""]||[[Hitcoefficient stringValue] isEqualToString:@""]) {
//        ||[[damagerate stringValue] isEqualToString:@""]||[[escaperate stringValue] isEqualToString:@""]||[[Blockrate stringValue] isEqualToString:@""]||[[Critrate stringValue] isEqualToString:@""]
        NSAlert *alert=[[NSAlert alloc] init];
        //[NSAlert alertWithMessageText:@"111" defaultButton:@"11" alternateButton:@"33" otherButton:@"22"informativeTextWithFormat:@"44"];
        [alert addButtonWithTitle:@"OK"];
        
        
        [alert setMessageText:@"错误"];
        
        [alert setInformativeText:@"错误，值不能为空"];
        
        [alert setAlertStyle:NSWarningAlertStyle];
        [alert beginSheetModalForWindow:_window modalDelegate:self didEndSelector:nil contextInfo:nil];
        return;

    }
    
    m_static =[[StaticPro alloc] init];

    
    m_static.base_hp=[NSNumber numberWithInt:[basehp intValue]];
//    m_static.base_strike=[NSNumber numberWithInt:[basecri intValue]];
    m_static.base_attack=[NSNumber numberWithInt:[baseattack intValue]];
    m_static.defense_cf=[NSNumber numberWithInt:[Defensecoefficient intValue]];
    m_static.tenacity_cf=[NSNumber numberWithInt:[Toughnessproperties intValue]];
    
    
    m_static.dodge_cf=[NSNumber numberWithInt:[Dodgecoefficient intValue]];
    m_static.parry_cf=[NSNumber numberWithInt:[Blockcoefficient intValue]];
    m_static.deflect_cf=[NSNumber numberWithInt:[Coefficientofskewness intValue]];
    m_static.strike_cf=[NSNumber numberWithInt:[Critratecoefficient intValue]];
    m_static.strike_result_cf=[NSNumber numberWithInt:[Critpowercoefficient intValue]];
    m_static.hitrate_cf=[NSNumber numberWithInt:[Hitcoefficient intValue]];
//    m_static.damage_rate=[NSNumber numberWithInt:[damagerate intValue]];
//    m_static.avoid_rate=[NSNumber numberWithInt:[escaperate intValue]];
//    m_static.parry_rate=[NSNumber numberWithInt:[Blockrate intValue]];
//    m_static.strike_rate=[NSNumber numberWithInt:[Critrate intValue]];
    
    [m_static setStaticProperties];
    
    
}
-(IBAction)setcomplete:(id)sender{
//    NSLog(@"%ld",[person count]);
//    for (int i=0;i<[person count] ; i++) {
//
//        if ([((Person*)[sceneInfo.enemyArr objectAtIndex:i]).m_id intValue]==[selectPet intValue]) {
//            [self setPersonValue:((Person *)[sceneInfo.enemyArr objectAtIndex:i])];
//            
//            return;
//        }
//    }
}

-(void)setPersonValue:(Person *)sender{
//    m_person=sender;
//    
//    
//    if (m_person.isSave) {
//      //  [typeId setStringValue:m_person.m_petid];
//        
//        [monsterName setStringValue:m_person.m_name];
//        
//        [defence setStringValue:m_person.m_defense];
//        
//        [attack setStringValue:m_person.m_attack];
//        
//        [hit setStringValue:m_person.m_hittarget];
//        
//        [tenacity setStringValue:m_person.m_tenacity];
//        
//        [dodge setStringValue:m_person.m_dodge];
//        
//        [parry setStringValue:m_person.m_parry];
//        
//        [Crit setStringValue:m_person.m_strike];
//        
//        [Critpower setStringValue:m_person.m_strike_result];
//        
//        [Attackrange setStringValue:m_person.m_range];
//        
//        [Movementspeed setStringValue:m_person.m_speed];
//        
//        [basehp setStringValue:m_person.base_hp];
//        
//      //  [basecri setStringValue:m_person.base_strike];
//        
//        [baseattack setStringValue:m_person.base_attack];
//        
//        [Defensecoefficient setStringValue:m_person.defense_cf];
//        
//        [Toughnessproperties setStringValue:m_person.tenacity_cf];
//        
//        [Dodgecoefficient setStringValue:m_person.dodge_cf];
//        
//        [Blockcoefficient setStringValue:m_person.parry_cf];
//        
//        [Coefficientofskewness setStringValue:m_person.deflect_cf];
//        
//        [Critratecoefficient setStringValue:m_person.strike_cf];
//        
//        [Critpowercoefficient setStringValue:m_person.strike_result_cf];
//        
//        [Hitcoefficient setStringValue:m_person.hitrate_cf];
//        
//        [damagerate setStringValue:m_person.damage_rate];
//        
//        [escaperate setStringValue:m_person.avoid_rate];
//        
//        [Blockrate setStringValue:m_person.parry_rate];
//        
//        [Critrate setStringValue:m_person.strike_rate];
//        
//    //    [level setStringValue:m_person.m_level];
//    }else{
//       // [typeId setStringValue:@"0"];
//        
//        [monsterName setStringValue:@"0"];
//        
//        [defence setStringValue:@"0"];
//        
//        [attack setStringValue:@"0"];
//        
//        [hit setStringValue:@"0"];
//        
//        [tenacity setStringValue:@"0"];
//        
//        [dodge setStringValue:@"0"];
//        
//        [parry setStringValue:@"0"];
//        
//        [Crit setStringValue:@"0"];
//        
//        [Critpower setStringValue:@"0"];
//        
//        [Attackrange setStringValue:@"0"];
//        
//        [Movementspeed setStringValue:@"0"];
//        
//        [basehp setStringValue:@"0"];
//        
//     //   [basecri setStringValue:@"0"];
//        
//        [baseattack setStringValue:@"0"];
//        
//        [Defensecoefficient setStringValue:@"0"];
//        
//        [Toughnessproperties setStringValue:@"0"];
//        
//        [Dodgecoefficient setStringValue:@"0"];
//        
//        [Blockcoefficient setStringValue:@"0"];
//        
//        [Coefficientofskewness setStringValue:@"0"];
//        
//        [Critratecoefficient setStringValue:@"0"];
//        
//        [Critpowercoefficient setStringValue:@"0"];
//        
//        [Hitcoefficient setStringValue:@"0"];
//        
//        [damagerate setStringValue:@"0"];
//        
//        [escaperate setStringValue:@"0"];
//        
//        [Blockrate setStringValue:@"0"];
//        
//        [Critrate setStringValue:@"0"];
//        
//      //  [level setStringValue:@"0"];
//    }
//    
    
}
-(IBAction)selectCounts:(id)sender{
    [tableView reloadData];
}
-(IBAction)selectMonster:(id)sender{

}
-(IBAction)writeFinished:(id)sender{
    
    if ([[countofenemy stringValue]intValue]==[sceneInfo.enemyArr count]) {
        return;
    }else if([[countofenemy stringValue]intValue]>[sceneInfo.enemyArr count]){
        for (unsigned long i=[sceneInfo.enemyArr count]; i<[[countofenemy stringValue] intValue]; i++) {
            [enemycount insertItemWithObjectValue:[NSString stringWithFormat:@"%ld",i+1] atIndex:i];
            NSMutableArray *nmarr=[[NSMutableArray alloc] init];
            [sceneInfo.enemyArr addObject:nmarr];
        }
    }else if([[countofenemy stringValue] intValue]<[sceneInfo.enemyArr count]){
        for (unsigned long i=[[countofenemy stringValue] intValue]; i<[sceneInfo.enemyArr count]; i++) {
            [sceneInfo.enemyArr removeObjectAtIndex:i];
            [enemycount removeItemAtIndex:i];
        }
    }
}
-(IBAction)makefile:(id)sender{
    if ([[resName stringValue] isEqualToString:@""]||[[up stringValue] isEqualToString:@""]||[[down stringValue] isEqualToString:@""]||[[left stringValue] isEqualToString:@""]||[[right stringValue] isEqualToString:@""]||[[gold stringValue] isEqualToString:@""]||[[exp stringValue] isEqualToString:@""]||[[[sceneType selectedCell] stringValue] isEqualToString:@""]||[[samePeople stringValue] isEqualToString:@""]||[[delaytime stringValue] isEqualToString:@""]||[[[enemycount selectedCell] stringValue] isEqualToString:@""]||[[[enemylist selectedCell] stringValue] isEqualToString:@""])
    {
        NSAlert *alert=[[NSAlert alloc] init];

        [alert addButtonWithTitle:@"OK"];

        
        [alert setMessageText:@"错误"];
        
        [alert setInformativeText:@"错误，值不能为空"];
        
        [alert setAlertStyle:NSWarningAlertStyle];
         [alert beginSheetModalForWindow:_window modalDelegate:self didEndSelector:nil contextInfo:nil];
        return;
    }
    [sceneInfo.enemyArr insertObject:[NSNumber numberWithInt:[samePeople intValue]] atIndex:0];
    [sceneInfo.enemyArr  insertObject:[NSNumber numberWithInt:[delaytime intValue]] atIndex:1];
    sceneInfo.resourceName=[resName stringValue];
    sceneInfo.up=[up stringValue];
    sceneInfo.down=[down stringValue];
    sceneInfo.left=[left stringValue];
    sceneInfo.right=[right stringValue];
    sceneInfo.gold=[gold stringValue];
    sceneInfo.exp=[exp stringValue];
    sceneInfo.sceneId=[sceneId stringValue];
    sceneInfo.sceneType=[[sceneType selectedCell] stringValue];
    SBJsonWriter *write=[[SBJsonWriter alloc] init];
    NSMutableDictionary *dict=[[NSMutableDictionary alloc] init];
    [dict setValue:sceneInfo.resourceName forKey:@"iname"];
    [dict setValue:[NSNumber numberWithInt:[sceneInfo.gold intValue]] forKey:@"silver"];
    [dict setValue:[NSNumber numberWithInt:[sceneInfo.exp intValue]]forKey:@"exp"];
   // [dict setValue:sceneInfo.sceneType forKey:@"typeid"];
    NSMutableArray *enearr=[[NSMutableArray alloc] init];
    [enearr addObject:sceneInfo.enemyArr];
    [dict setValue:enearr forKey:@"mlist"];
 //   [dict setValue:@"" forKey:@"condition"];
    NSMutableArray *udArr=[[NSMutableArray alloc] init];
    [udArr addObject:[NSNumber numberWithInt:[sceneInfo.up intValue]]];
    [udArr addObject:[NSNumber numberWithInt:[sceneInfo.down intValue]]];
    [udArr addObject:[NSNumber numberWithInt:[sceneInfo.left intValue]]];
    [udArr addObject:[NSNumber numberWithInt:[sceneInfo.right intValue]]];
    [dict setValue:udArr forKey:@"udlf"];

    NSString *str=[write stringWithObject:dict];

     NSString *strs=[[NSString alloc] initWithFormat:@"http://127.0.0.1:11008/addinstance?json=%@",str];
    NSLog(@"%@",strs);
    NSURL *url = [NSURL URLWithString:[strs stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
	[request setTimeOutSeconds:5];
	[request setDelegate:self];
	[request startAsynchronous];
    [sceneInfo.enemyArr exchangeObjectAtIndex:0  withObjectAtIndex:2];
    [sceneInfo.enemyArr  removeObjectAtIndex:2];
    [sceneInfo.enemyArr  removeObjectAtIndex:1];
}

-(IBAction)sameMonior:(id)sender{
    
    
}
-(IBAction)delaytime:(id)sender{
    
}
@end
