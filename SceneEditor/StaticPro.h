//
//  StaticPro.h
//  SceneEditor
//
//  Created by lh on 14-3-20.
//  Copyright (c) 2014年 lh. All rights reserved.
//  www.9miao.com

#import <Foundation/Foundation.h>

@interface StaticPro : NSObject

@property (retain,nonatomic)NSNumber * base_hp;//标准生命
@property (retain,nonatomic)NSNumber * base_attack;//标准攻击
@property (retain,nonatomic)NSNumber * base_strike;//标准暴击值
@property (retain,nonatomic)NSNumber * defense_cf;//防御系数
@property (retain,nonatomic)NSNumber * dodge_cf;//闪避系数
@property (retain,nonatomic)NSNumber * tenacity_cf;//韧性系数
@property (retain,nonatomic)NSNumber * parry_cf;//格挡系数
@property (retain,nonatomic)NSNumber * deflect_cf;//偏斜系数
@property (retain,nonatomic)NSNumber * strike_cf;//暴击率系数
@property (retain,nonatomic)NSNumber * strike_result_cf;//暴击威力系数
@property (retain,nonatomic)NSNumber * hitrate_cf;//命中系数

@property (retain,nonatomic)NSNumber * damage_rate;//伤害率
@property (retain,nonatomic)NSNumber * avoid_rate;//躲避率
@property (retain,nonatomic)NSNumber * parry_rate;//格挡率
@property (retain,nonatomic)NSNumber * strike_rate;//暴击率
@property (retain,nonatomic)NSMutableDictionary *staticProperties;
-(void)setStaticProperties;
@end
