//
//  main.m
//  SceneEditor
//
//  Created by lh on 14-1-9.
//  Copyright (c) 2014年 lh. All rights reserved.
//  www.9miao.com

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[])
{
    return NSApplicationMain(argc, argv);
}
