//
//  Person.m
//  SceneEditor
//
//  Created by lh on 14-1-9.
//  Copyright (c) 2014年 lh. All rights reserved.
//  www.9miao.com

#import "Person.h"
#import "ASIHTTPRequest.h"
@implementation Person
@synthesize color;
@synthesize btype;
@synthesize coatt;
@synthesize dou;
@synthesize ske;
@synthesize m_level;
@synthesize m_id;
@synthesize m_name;
@synthesize m_attack;
@synthesize m_defense;
@synthesize m_hittarget;
@synthesize m_attack_speed;
@synthesize m_dodge;
@synthesize m_hp;
@synthesize m_parry;
@synthesize m_petid;
@synthesize m_range;
@synthesize m_speed;
@synthesize m_strike;
@synthesize m_strike_result;
@synthesize m_tenacity;
@synthesize avoid_rate;
@synthesize base_attack;
@synthesize base_hp;
@synthesize base_strike;
@synthesize damage_rate;
@synthesize defense_cf;
@synthesize deflect_cf;
@synthesize dodge_cf;
@synthesize hitrate_cf;
@synthesize parry_cf;
@synthesize parry_rate;
@synthesize res_icon;
@synthesize res_resource;
@synthesize skill_Id;
@synthesize strike_cf;
@synthesize strike_rate;
@synthesize strike_result_cf;
@synthesize tenacity_cf;
@synthesize isSave;
@synthesize properties;
@synthesize skillId;
@synthesize staticProperties;
-(void)setProperties{
    properties=[[NSMutableDictionary alloc] init];


    [properties setValue:m_name forKey:@"pname"];

    [properties setValue:m_attack forKey:@"att"];
    [properties setValue:m_hp forKey:@"hp"];
    [properties setValue:m_defense forKey:@"def"];
    [properties setValue:m_hittarget forKey:@"dex"];
    [properties setValue:m_tenacity forKey:@"tou"];
    
//    [properties setValue:m_dodge forKey:@"dodge"];
    [properties setValue:m_parry forKey:@"par"];
    [properties setValue:m_strike forKey:@"cri"];
    [properties setValue:m_strike_result forKey:@"crp"];
    [properties setValue:m_range forKey:@"rng"];
    [properties setValue:m_speed forKey:@"mov"];
    [properties setValue:m_attack_speed forKey:@"spd"];
//    [properties setValue:base_hp forKey:@"base_hp"];
//    [properties setValue:base_attack forKey:@"base_attack"];
//    [properties setValue:base_strike forKey:@"base_strike"];
//    [properties setValue:defense_cf forKey:@"defense_cf"];
//    [properties setValue:dodge_cf forKey:@"dodge_cf"];
//    
//    
//    [properties setValue:tenacity_cf forKey:@"tenacity_cf"];
//    [properties setValue:parry_cf forKey:@"parry_cf"];
//    [properties setValue:deflect_cf forKey:@"deflect_cf"];
//    [properties setValue:strike_cf forKey:@"strike_cf"];
//    [properties setValue:strike_result_cf forKey:@"strike_result_cf"];
//    [properties setValue:hitrate_cf forKey:@"hitrate_cf"];
//    [properties setValue:damage_rate forKey:@"damage_rate"];
//    [properties setValue:avoid_rate forKey:@"avoid_rate"];
//    [properties setValue:parry_rate forKey:@"parry_rate"];
//    [properties setValue:strike_rate forKey:@"strike_rate"];
    [properties setValue:res_icon forKey:@"resourceid"];
    [properties setValue:m_hp forKey:@"hp"];
    [properties setValue:dou forKey:@"dou"];
    [properties setValue:ske forKey:@"ske"];
    
    NSNumber *skillNum;
    if ([skill_Id isEqualToString:@"弹指神符"]) {
        skillNum=[NSNumber numberWithInt:1];
        
        
    }else if([skill_Id isEqualToString:@"生死符"]){
        skillNum=[NSNumber numberWithInt:2];
    }else if([skill_Id isEqualToString:@"地裂山崩"]){
        skillNum=[NSNumber numberWithInt:3];
    }else if ([skill_Id isEqualToString:@"指点江山"]){
        skillNum=[NSNumber numberWithInt:4];
    }else if([skill_Id isEqualToString:@"范围加血"]){
        skillNum=[NSNumber numberWithInt:5];
    }
    NSNumber *colorStr;
    if ([color isEqualToString:@"白色"]) {
        colorStr=[NSNumber numberWithInt:1];
        
    }else if([color isEqualToString:@"蓝色"]){
        colorStr=[NSNumber numberWithInt:2];
    }else if([color isEqualToString:@"紫色"]){
        colorStr=[NSNumber numberWithInt:3];
    }else if ([color isEqualToString:@"橙色"]){
        colorStr=[NSNumber numberWithInt:4];
    }
    
    NSNumber *btypeStr;
    if ([btype isEqualToString:@"近战"]) {
        btypeStr=[NSNumber numberWithInt:1];
    }else if ([btype isEqualToString:@"远程"]){
        btypeStr=[NSNumber numberWithInt:2];
    }else if ([btype isEqualToString:@"加血"]){
        btypeStr=[NSNumber numberWithInt:3];
    }

    
    NSNumber *coattStr;
    if ([coatt isEqualToString:@"改变目标"]) {
        coattStr=[NSNumber numberWithInt:1];
    }else if ([coatt isEqualToString:@"随机目标"]){
        coattStr=[NSNumber numberWithInt:2];
    }else if([coatt isEqualToString:@"锁定目标"]){
        coattStr=[NSNumber numberWithInt:3];
    }
    [properties setValue:coattStr forKey:@"coatt"];
    [properties setValue:btypeStr forKey:@"btype"];
    [properties setValue:colorStr forKey:@"color"];
    NSMutableArray *skillArr=[[NSMutableArray alloc] init];
    [skillArr addObject:skillNum];
    [properties setObject:skillArr forKey:@"skilllist"];
    
    
    
    SBJsonWriter *writer=[[SBJsonWriter alloc] init];
    
    NSString *jsonStr=[writer stringWithObject:properties];
    
    NSLog(@"%@",jsonStr);
    
    NSString *str=[[NSString alloc] initWithFormat:@"http://127.0.0.1:11008/addmonster?json=%@",jsonStr];
    //[NSString stringWithFormat:@"http://172.16.18.25:11008/addmonster?json=%@",jsonStr];
    NSLog(@"%@",str);
    
    NSURL *url = [NSURL URLWithString:[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
	[request setTimeOutSeconds:5];
	[request setDelegate:self];
	[request startAsynchronous];
//    NSURLRequest * request = [[NSURLRequest alloc]initWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:60];
//    [[NSURLConnection connectionWithRequest:request delegate:self] start];
//    NSHTTPURLResponse * response = nil;
//    NSData * returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
//    NSString * returnStr = [[NSString alloc]initWithData:returnData encoding:4];
  //  NSLog(@"%@",returnStr);
    
}
-(void)setStaticProperties{
    [staticProperties setValue:base_hp forKey:@"base_hp"];
    [staticProperties setValue:base_attack forKey:@"base_attack"];
    [staticProperties setValue:base_strike forKey:@"base_strike"];
    [staticProperties setValue:defense_cf forKey:@"defense_cf"];
    [staticProperties setValue:dodge_cf forKey:@"dodge_cf"];
    
    
    [staticProperties setValue:tenacity_cf forKey:@"tenacity_cf"];
    [staticProperties setValue:parry_cf forKey:@"parry_cf"];
    [staticProperties setValue:deflect_cf forKey:@"deflect_cf"];
    [staticProperties setValue:strike_cf forKey:@"strike_cf"];
    [staticProperties setValue:strike_result_cf forKey:@"strike_result_cf"];
    [staticProperties setValue:hitrate_cf forKey:@"hitrate_cf"];
    [staticProperties setValue:damage_rate forKey:@"damage_rate"];
    [staticProperties setValue:avoid_rate forKey:@"avoid_rate"];
    [staticProperties setValue:parry_rate forKey:@"parry_rate"];
    [staticProperties setValue:strike_rate forKey:@"strike_rate"];

}
@end
